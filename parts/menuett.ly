\tocItem \markup "24 Menuett"
  \score {

    \new GrandStaff <<
      \new Staff = "violin" {
	\key f \major
        \time 3/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            c4 f2 (| % 1
            f4.) e8 e4 | % 2
            f4 g8 a bes4 \((| % 3
            bes4) c\) r | % 4
            g8 f e4 f | % 5
            e'2. | % 6
            bes8 a g4 f | % 7
            f4 (e) r | % 8
          }
          \repeat volta 2 {
            r4 r g | % 9
            c4. bes8 a4 | % 10
            d8 c b c bes a | % 11
            g4 c, r | % 12
            bes'4. c8 bes4 | % 13
            a4 d2 | % 14
            f,4. e16 d e4 | % 15
            f2. | % 16
          }
	}
      }
      
      \new Staff = "violoncello" {
	\key f \major
        \time 3/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \repeat volta 2 {
            a4.-1\downbow bes8-3 a4-1 | % 1
            g4-0 c2-4 | % 2
            f,2-4 (g4-0)\upbow | % 2
            e2-3 f4-4 | % 3
            g8-0 a-1 bes4-3 a-1 | % 4
            e2-3 f4-4 | % 5
            g8-0 a-1 bes4-3 a-1 | % 6
            g2.-0 | % 7
          }
          \repeat volta 2 {
            c,4-0\downbow f2-4 | % 8
            d4.-1 e8-3 f4-4 | % 9
            c4-0 f2-4 | % 10
            d4.-1 e8-3 f4-4 | % 11
            g4-0 a4.-1 bes8-3 | % 12
            c4-4 bes8-3 a-1 g-0 f-4 | % 13
            g4-0 g2 | % 14
            f2.-4 | % 15
          }
	}
      }
    >>
    \header { piece = \markup \bold { \caps "24 Menuett" } }
  }