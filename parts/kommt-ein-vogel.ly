\tocItem \markup "17 Kommt ein vogel"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
        \time 3/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
        \repeat volta 2 {
          \partial 4 r4 |
          r2 c4\staccato |
          c4\> (b)\! r |
          r4 r f\staccato |
          f4\> (e)\! r |
          r4 c c' |
          c4 (b) a |
          a4 (g) f |
          }
        \alternative {
          {  e2.\> | } % fixme
          { e4.\! (a8) g f | }
        }
        e4. (a8) g f |
        e2\fermata \bar "|."
      }
      }
      
      \new Staff = "violoncello" {
	\key c \major
        \time 3/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
        \repeat volta 2 {
          \partial 4 e8-1 f-3 |
          g4-4 e-1 e |
          e4 (d-0) d8 e-1 |
          f4-3 d-0 a-1 |
          g2-0 e8-3 f-4 |
          g4-0 e-3 e |
          e4 (d-1) d8 e-3 |
          f4-4 (b-3) b |
        }
        \alternative {
          { c2.-4 |}
          {c2-4 b4-3 |}
        }
        c2-4 b4-3 |
        c4-4 c,4-0\fermata \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "17 Kommt ein vogel" } }
  }