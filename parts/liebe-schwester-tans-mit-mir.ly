\tocItem \markup "16 Liebe Schwester, Tanz Mit Mir"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          g8 b d,4 |
          a'8 c d,4 |
          fis8 a d, fis |
          g8 b d,4 |
          e8 g  c, e |
          fis8 a d d |
          b8 g c e, |
          fis8 d g4 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
        \time 2/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          d8-0\downbow (g-4) g g |
          fis8-3 a-0 d,4-0 |
          d8 (fis-3) a-0 c-3 |
          b8-1 d-4 g,4-4 |
          b8-1 b c4-3 |
          a8-0 a d4-4 |
          g,8-4 g a-0 a |
          fis8-3 fis g4-4 \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "16 Liebe Schwester, Tanz Mit Mir" } }
  }