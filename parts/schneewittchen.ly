\tocItem \markup " 5. Schneewittchen"
  \score {
    % wiegenlied

    \new GrandStaff <<
      \new Staff = "violin" {
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
				%staffkeysig
	  \key d \major 
				%barkeysig: 
	  \key d \major 
				%bartimesig: 
	  \time 4/4 
	  \partial 4 fis4   | % 1
	  e4 fis g b      | % 2
	  a2. fis4      | % 3
	  e fis g b      | % 4
	  a2. b4      | % 5
	  a2. b4      | % 6
	  a2 r4 fis      | % 7
	  e fis g b      | % 8
	  a2. a4      | % 9
	  b a g e     | % 10
	  d4 fis d  \bar "|." 
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "cello"
	\relative c {
	  \key d \major 
				%barkeysig: 
	  \key d \major 
				%bartimesig: 
	  \time 4/4
	  \partial 4 r4 |   | % 1
	  R1                | % 2
	  r2 r4 a'4-0       | % 3
	  b4-1 a-0 g-4 e-1  | % 4
	  d-0 (fis)-3 fis2  | % 5
	  e4-1 (g)-4 g2     | % 6
	  fis4-3 (a)-0 a\staccato (a\staccato)      | % 7
	  b-1 a-0 g-4 e-1   | % 8
	  d-0 (fis)-3 fis2  | % 9
	  a-0 a             | % 10
	  d,2.-0  \bar "|." % 11
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "5. Schneewittchen" } }
  }