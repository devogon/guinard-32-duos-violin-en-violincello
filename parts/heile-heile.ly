\tocItem \markup " 2. Heile, Heile"
  \score {
    \new GrandStaff <<
      \new Staff = "violin" {
	    \key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative d''
	    a2 fis'2 |
	    a'2. r4 |
	    a'2 fis'4 fis'4 |
	    a'2. r4 |
	    fis'2 d'2 |
	    fis'2 a'4 fis'4 |
	    a'1 ~ |
	    a'4 fis'4 a'2 \bar "|."
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c'
	a4-0 a4 b4-1 b4 |
	a2-0 fis2-3 |
	a2-0 b2-1 |
	a2-0 fis2-3 |
	a4-0 a4 a4 a4 |
	b2.-1 b4 |
	a4-0 g4-4 fis4-3 e4-1 |
	d4-0 d4 d2 |
      }
    >>
    \header { piece = \markup \bold { \caps "2. Heile, Heile" } }
  }