\tocItem \markup "13 Sur Le Pont D'Avignon"
  \score {
    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \partial 2 b8 b b4 |
          d,8 d d4 g8\staccato fis (g a) |
          b8 b b4 c'8\staccato b (a g) |
          fis8 e d4 d8\staccato c (b a) |
          b8 c b4 a8\staccato g (fis e) |
          fis8 g fis4 r8 a8 (b c) |
          b8 a g4 r8 b8 (a g) |
          fis8 e d4 d'8\staccato c (b a) |
          b8 b b4 d,8 d d4 |
          g8 g g4 \bar ":|"
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          r2 |
          r2 g4-4 g |
          g2 a4-0 a |
          a2 b4-1 (c-3) |
          d4-4 (g,-4) fis-3 (g-4) |
          a4-0 (b-1) g-4 g |
          g2 a4-0 a |
          a2 b4-1 (c-3) |
          d4-4 (g,-4) a-0 fis-3 |
          g2-4 \bar ":|"
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "13 Sur Le Pont D'Avignon" } }
    % \layout {
    %   #(layout-set-staff-size 22)
    %   }
  }