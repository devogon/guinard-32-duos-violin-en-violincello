\tocItem \markup "11 Ihr Kinderlein Kommet"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key d \major
%	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \partial 2 a2 |
          a2 a2 |
          b2 a2 |
          a1 |
          fis1 |
          r2 a2 |
          g2. fis4 |
          e2 e2 |
          d1 (|
          d2.) r4 |
          r1 |
          d2 (d8) e fis g |
          a2 d2 (|
          d2) cis |
          b2 b |
          a2 b4 cis |
          d1 (|
          d2) cis4 b |
          a1 (|
          a4) b2 cis4 |
          d1 |
          a2 g |
          fis1 (|
          fis2) \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
%	\set Staff.instrumentName = "violoncello"
	\relative c' {
          r2 |   % 1
          r1 |   % 2
          r1 |   % 3
          r1 |   % 4
          r2 r4 a-0 |   % 5
          a2 fis4-3 a-0 | % 6
          a2 fis4-3 a-0 | % 7
          g2-4 e4-1 g-4 | % 8
          fis2-3 r4 a-0 |
          a2 fis4-3 a-0 |
          a2 fis4-3 a |
          g2-4 e4-1 g-4 |
          fis2-3 r4 fis-3 |
          e2-1 e4 e |
          g2-4 g4 g |
          fis2-3 fis4 fis |
          b2.-1 b4 |
          a2-0 a4 a |
          d2-4 a4-0 fis-3 |
          a2-0 g4 -4(e-1) |
          d1-0 |
          d1 |
          <d-0 a'-0>1 (|
          <d a'>2) \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "11 Ihr Kinderlein Kommet" } }
  }