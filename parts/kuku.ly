\tocItem \markup "19 Kuku"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key f \major
        \time 3/8
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
		\repeat volta 2 {
                  g8 f g |
                  a8. c16 e8 |
                  g,8 f g |
                  f8. c'16 a8|
                  g8 f g |  % 5
                  a4 c16 a |
                  bes8 a bes |
                  c4. (|
                  c8) bes a |
                  g4. (| %10
                  g8) f e16 d |
                  c4 r8 |
                  r8 f g |
                  a8. c16 f,8 (|
                  a8) a bes |  % 15
                  c8. a16 c a |
                  c8 a r |
                  c8 a r |
                  bes8 a g |
                  f4. \bar ":|"
	}
      }
}
      
      \new Staff = "violoncello" {
	\key f \major
        \time 3/8
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          \repeat volta 2 {
            r4. |
            r4. |
            r4. |
            r4. |
            c8-3 a-0 r |
            c8-3 a-0 r |
            g8-4 f-3 g-4 |
            f4-3 r8 |
            g8-4 g a-0 |
            bes4-1 g8-4 |
            a8-0 a bes-1 |
            c4-3 a8-0 |
            c8-3 a-0 r |
            c8-3 a-0 r |
            bes8-1 a-0 g-4 |
            f4.-3 (
            f8) r8 c8-4 |
            f8-3 r r |
            d8-0 e-1 c-4 |
            f4.-3 \bar ":|"
            }
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "19 Kuku" } }
  }