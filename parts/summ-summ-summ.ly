\tocItem \markup "10 Summ, Summ, Summ"
  \score {
    % wiegenlied

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
		b4 c |
                d4 r |
                d4 c |
                b4 r |
                b4 c |
                d4 r |
                c8 b a c |
                b8 a g b |
                d,2 (|
                d4.) r8 |
                g8 a b g |
                fis8 g a fis |
                e8 fis g e |
                d8 e fis d |
                c8 d e fis |
                g8 a b c |
                d4 c |
                b2 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
		r2 |
                r2 |
                r2 |
                r2 |
                d'4-4 c-3 |
                b4-1 r |
                a8-0 b-1 c-3 a-0 |
                g4-4 r |
                b8-1 c-3 d-4 b-1 |
                a8-0 b-1 c-3 a-0 |
                b8-1 c-3 d-4 b-1 |
                a8-0 b-1 c-3 a-0 |
                d4-4 c-3 |
                b4-1 r |
                a8-0\downbow b-1 c-3 a-0 |   
                g4-4 r |
                <g,-4 g'-0>2\upbow | 
                <g g'>2\downbow \bar "|."  
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "10 Summ, Summ, Summ" } }
  }