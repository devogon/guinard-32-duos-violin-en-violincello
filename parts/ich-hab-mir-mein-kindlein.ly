\tocItem \markup "30 Ich Hab' Mir Mein Kindlein"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
        \time 3/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            \partial 4 r4 | % 1
            c8 (d) e f g4 | % 2
            g4 (f) f | % 3
            d8 e f g a b | % 4
            c8 (b) c d c4 | % 5
            c8 (b) c d c4 | % 6
            b8 (a) b c b4 | % 7
            d8 (cis) d e d4 | % 8
            c!2 % 9
            }
          \repeat volta 2 {
            r4 | % 10
            e8 d c b a g | % 11
            g4 (f) f | % 12
            d'8 c b a g f | % 13
            e2 r4 | % 14
            c8 (d) e f g4 | % 15
            g4 (f) f | % 16
            g8 (fis) g a g4 | % 17
            c2 % 18
          }
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
        \time 3/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
           \repeat volta 2 {
             \partial 4 e4-1 | % 1
             g4-4 (e-1) e | % 2
             e4 (d-0) d | % 3
             f4-3 (d-0) d | % 4
             e2-1 e4 | % 5
             g4-4 (c,-4) c | % 6
             e4-1 (d-0) d | % 7
             f4-3 (b,-3) b | % 8
             c2-4  % 9 
             }
           \repeat volta 2 {
             g4-0 | % 10
             g4 (c-4) c | % 11
             b4-3 (d-0) d | % 12
             d4 f-3 (a-0)  | % 13
             g2-4 e4-1 | % 14
             g4-4 (c,-4) c | % 15
             e4-1 (d-0) d | % 16
             f?4-3 (b,-3) b | % 17
             c2-4  % 18
            }
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "30 Ich Hab' Mir Mein Kindlein" } }
  }