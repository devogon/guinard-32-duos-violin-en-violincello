\tocItem \markup "23 Gott Ist's, Der Regiert!"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
        \time 4/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            c2 g' | % 1
            f4 e d2 | % 2
            c4 c e c | % 3
            d2 r | % 4 
            r2 g4 c | % 5 
            b4 a g2 | % 6
            a4 a g b | % 7
            c2 r | % 8
            b2 b | % 9
            c4 a g f | % 10
            e4 c d2 (| % 11
            d4) c g'2 | % 12
            f4 e d2 | % 13
            c4 c e c | % 14
            d2 r | % 15
            g2 c | % 16
            b4 a g2 | % 17
            a4 a g b | % 18
            c1 ( | % 19
            c4) b c2  | % 20
            e,4 e f d | % 21
            c1 | % 22
          }
        }
      }
      
      \new Staff = "violoncello" {
	\key c \major
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          \repeat volta 2 {
            r1 |
            r1 |
            r1 |
            r1 | % 4
            c2-3 g-4 | % 5
            a4-0 b-1 c2-3 | % 6
            e,4-1 e f-3 d-0 | % 7
            c2-4 r | % 8
            g'2-4 g | % 9
            e4-1 f-3 g2-4 | % 10
            e4-1 g-4 f-3 f | % 11
            e2-1 r | % 12
            g2-4 f | % 13
            e4-1 d-0 c2-4 | % 14
            a4-1 b-3 c-4 a-1 | % 15
            g2-0 r | % 16
            g4 c-4 d-0 e-1 | % 17
            f2-3 e-1 | % 18
            c4-4 d-0 e-1 f-3 | % 19
            d2-0 c-4 | % 20
            r1 | % 21
            r1 | % 22
          }
	}	
      }
    >>
    \header { piece = \markup \bold { \caps "23 Gott Ist's, Der Regiert!" } }
  }