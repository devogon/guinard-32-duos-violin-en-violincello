\tocItem \markup "12 Auf Unsrer Wiese Gehet Was"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
	\partial 4 r4 | %\bar "|:"
        \repeat volta 2 {
          r2 r4 g |
          c4 c c c |
          b2 a |
        }
        \alternative {
          { g2 r2 | }
          { g2 r2 | }
         }
        r1 |
        a4 a a a |
        d4 d d2 |
        b4 b c a |
        g4 g a fis |
        fis2 g4 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \partial 4 d4-0 | %\bar "|:"
          \repeat volta 2 {
          g4-4 g g g |
          fis4-3 e-1 d2-0 |
          e4-1 e fis-3 fis |
        }
          \alternative {
            { g2-4 g4 d-0 | }
            { g2-4 g4 r | }
            }
          b4-1 b b b |
          c4-3 c c2 |
          a4-0 a a a |
          d4-4 d d2 |
          b4-1 b c-3 a-0 |
          a2 g4-4 \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "12 Auf Unsrer Wiese Gehet Was" } }
  }