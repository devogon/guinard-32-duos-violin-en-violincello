\tocItem \markup "No. Piecename"
  \score {
    % wiegenlied

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c {
				% music in here
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
				% music in here
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "No. Piecename" } }
  }