\tocItem \markup "25 Jäger und hase"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key f \major
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
		c8 (a) a4 | % 1
                g8 \((c,)\) c4\fermata | % 2
                c'8 \((a)\) a \((f)\) | % 3 CHECK THESE NOTES
                g8 \((d)\) c4\fermata | % 4
                \repeat volta 2 {
                  c'8 \((a)\) a \((f)\) | % 5
                  g8 \((e)\) e \((c)\) | % 6
                  c8 \((f)\) f f | % 7
                  g8 \((a)\) a4 | % 8
                  c,8 \((f)\) f4 | % 9
                  f8 \((a)\) a4 | % 10
                  g8 f e d | % 11
                  c8 c c r | % 12
                  c'8 \((a)\) a \((a)\) | % 13
                  bes8 \((g)\) g4 | % 14
                  f8 f e g | % 15
                  g8 \((a)\) a4 | % 16
                  c8 \((a)\) a \((f)\) | % 17
                  g8 \((e)\) e \((c)\) | % 18
                  c8 f f \((f)\) | % 19
                  d8 \((e)\) f4 | % 20
                }
              }
      }
      
      \new Staff = "violoncello" {
	\key f \major
        \time 2/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          r2 | % 1
          r2 | % 2
          r2 | % 3
          r2 | % 4
          \repeat volta 2 {
            c,8-0 (f-4) f f | % 5
            e8-3 (g-0) g4 | % 6
            a8-1 a bes-3 g-0 | % 7
            g8 (f-4) f4 | % 8
            c'8-4 (a-1) a (f-4) | % 9
            f8 (d-1) d4 | % 10
            bes'8-3 a-1 g-0 f-4 | % 11
            e8-3 d-1 c4-0 | % 12
            c8 (f-4) f f | % 13
            e8-3 (g-0) g4 | % 14
            a8-1 a bes-3 g-0 | % 15
            g8 (f-4) f4 | % 16
            c8-0 (f-4) f4 | % 17
            e8-3 (g-0) g4 | % 18
            a8-1 a bes-3 g-0 | % 19
            g (f-4) f4 | % 20
          }
	}
      }
    >>
    \header { piece = \markup \bold { \caps "25 Jäger und hase" } }
  }