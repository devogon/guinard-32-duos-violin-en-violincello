\tocItem \markup " 1. Wiegenlied"
  \score {
    % wiegenlied

    \new GrandStaff <<
      \new Staff = "violin" {
	    \key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'
	fis4 g'4 a'2 |  % 1
	d'2 e'2 | % 2
	fis'4 g'4 a'4 d''4 | %  3
	d'2. e'4 |
	fis'4 e'4 d'2 ~ |
	d'4 g'4 fis'2 |
	d''2 a'2 |
	g'4 g'4 fis'2 \bar "|."
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c
	a'2-0 fis-3 |
	g4-4 g4  e2-1 |
	a4-0 a fis-3 fis |
	g4-4 g4  e2-1 |
	a4-0 a fis-3 d-0 |  % 5
	b4-1 b  a2-0 | % 6
	g4-4 a-0 fis-3 d-0 | % 7
	e-1 e d2-0 | % 8
      }
    >>
    \header { piece = \markup \bold { \caps "1. Wiegenlied" } }
%    \midi {}
%    \layout {}
  }