\tocItem \markup "21 ,,Schneie''"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key a \minor
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            c8\mf c e e |
            e8 e a4 |
            d,8 d f f |
            e8 e g4 |
            e8\> e16 e g8 g16 g | % 5
            c8 c16 c e4\! |
            r2- "G.P." |
            c8\p c, e g |
            g8 (f) e4 \bar ":|"
          }
	}
      }
      
      \new Staff = "violoncello" {
	\key a \minor
        \time 2/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          \repeat volta 2 {
            g8-4 g d-0 d |
            f8-3 f16 (e-1) d4-0 |
            f8-3 f d-0 d|
            g8-4 g16 (f-3) d4-0 |
            c'8-3 c16 c b8-1 b16 b | % 5
            a8-0 a16 a g4-4 |
            r2 |
            e8-1 e g-4 c,-4 |
            e8-1 (d-0) c4-4 \bar ":|"
          }
	}
      }
    >>
    \header { piece = \markup \bold { \caps "21 ,,Schneie''" } }
  }