\tocItem \markup "20 Aus dem himmel ferne"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \minor
        \time 4/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \repeat volta 2 {
            g4. f8 g4 a |
            bes2 c |
            d2 r4 g,4 (|
            g8) f8 g4 a bes (|
            bes4) a8 bes c4 d |
            r4 g,8 f g4 a |
            bes4. c8 bes4 aes |
            bes2 a |
            ees8 f g4 (g8) aes g f |
            bes2 r2 \bar ":|" 
          }
	}
      }
      
      \new Staff = "violoncello" {
	\key g \minor
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \repeat volta 2 {
            r1 |
            r1 |
            f4.-3 g8-4 f4-4 ees-1 |
            d2-0 c-4 |
            bes4-3 c8-4 d-0 e4-1 bes-3 |
            c2.-4 r4 |
            d4-0 f-3 g-4 f-3 |
            bes2-1 a4-0 g-4 |
            f4-3 f8 g-4 g4 f-3 |
            d2-0 r \bar ":|"
          }
	}
      }
    >>
    \header { piece = \markup \bold { \caps "20 Aus dem himmel ferne" } }
  }