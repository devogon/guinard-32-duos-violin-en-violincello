\tocItem \markup "26 Der Winter Ist Vergangen (FIXME - measures incorrect)"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
        \time 4/4
	\set Staff.instrumentName = #"violin"
        \set Score.extraNatural = ##f
	\clef treble
	\relative c'' {
          \partial 4
          \repeat volta 2 {
            %\partial 4
            d4 % 1
            b4 b a (fis) | % 2
            g2 g4 a | % 3
            fis4 e fis d | % 4
            }
          \alternative {
            {              
%            \set Timing.measureLength = #(ly:make-moment 3/4)
              fis2 (g4) } % 5
            { g2 r4  } % 6
          }
          \repeat volta 2 {
            r4 % 7
            d2 (g4) r | % 8
            d2 (a'4) r | % 9
            d,4. d'8 d2 (| % 10
            d4) e (d) d8 (c) | % 11
            b4 b a (fis) | % 12
            g2 g4 a | % 13
          }
          \alternative {
            { fis4 e fis d' | % 14
              b2 r4  } % 15
            { fis4 e d a'
              g2 r4  }
          }
          \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
        \time 4/4
	\clef bass
	\set Staff.instrumentName = #"violoncello"
	\relative c {
          \partial 4
          \repeat volta 2 {
            d4-0 | % 1
            g4-4 g a-0 (d-4) | % 2
            b2-1 b4 c-3 | % 3
            a4-0 g-4 a-0 fis-3 | % 4
          }
          \alternative {
            { g2-4 r4 } % 5
            { 
               \set Timing.measureLength = #(ly:make-moment 3 4)
              g2-4 r4 } % 6
          }
          \repeat volta 2 {
            \set Timing.measureLength = #(ly:make-moment 1 4)
            g8 (a) % 7
            \set Timing.measureLength = #(ly:make-moment 4 4)
            b4 b b a8 (b) | % 8
            c2 c4 c | % 9
            b4 g a c8 (b) | % 10
            a2 r4 d, | % 11
            g4 g a (d) | % 12
            b2 b4 c | % 13
          }
          \alternative {
            {          
              a4 g a fis g2 r4
            } 
            {
              \cadenzaOn
 \set Timing.measureLength = #(ly:make-moment 4 4)
              a4 g a fis \bar "|"
              g2 r4
            } 
          }
          \bar "|."
	}
      }
    >>
    \header { piece = \markup \bold { \caps "26 Der Winter Ist Vergangen (FIXME - measures incorrect)" } }
  }

