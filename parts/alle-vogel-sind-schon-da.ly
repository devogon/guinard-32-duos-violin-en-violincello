\tocItem \markup " 8 Alle Vögel Sind Schon Da"
  \score {
    % wiegenlied

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
		fis4 a8 (fis8) e8 (d8) e4 |
                fis4 d8 (fis8) a2 |
                r8 d,8 a'8 (fis8) e8 (d8) e4 |
                fis8 (e8) d8 (fis8) a4 d4 |
                b4 d8 b8 a2 |
                g4 b8 (g8) fis8 (e8) fis4 \((|
                fis8) e8\) fis4 e8 (d8) e4 | % legato "fis fis e" here
                d8 (cis8) d4 r2 |
                fis4 a8 (fis8) e8 (d8) e4 \((|
                e8) d8\) fis4 a8 b8 a8 g8 | % legato e-e-d here
                g2. r4 |
                r2 r4 fis4 |
                b4 d8 b8 a2 |
                g4 d8 g8 fis4 a8 (fis8) |
                e8 (d8) e4 fis4 d8 fis8 |
                a1\fermata  \bar "|."
                % music in here
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
		r1 |
                r1 |
                d4.-0 fis8-3 a4-0 d4-0 |
                b4-1 d8-0 b8-1 a2-0 |
                g4.-4 a8-0 fis4-3 d4-0 |
                e2-1 d2-0 |
                a'4-0 a4 g4-4 g4 |
                fis4-3 a8-0 (fis8-3) e2-1 |
                a4-0 a4 g4-4 g4 |
                fis4-3 a8-0 (g8-4) fis2-3 |
                d4.-0 e8-1 a4-0 d4-0 |
                b4-1 d8-0 b8-1 a2-0 |
                g4.-4 a8-0 fis4-3 d4-0 |
                e2-1 d2-0 (|
                d4) a4-0 d2-0 |
                <a-1 e'-1>1\fermata 
                \bar "|."
                
          
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "8 Alle Vögel Sind Schon Da" } }
  }