\tocItem \markup " 7. Winter Ade"
  \score {


    \new GrandStaff <<
      \new Staff = "violin" {
	\key c \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\time 3/4
	\relative c' {
	  r2 r4 | % 1
	  e4 (f) g | % 2
	  g2. | % 3
	  e4 (f) g | % 4
	  g4 f8 e8 f4 | % 5
	  d4 e8 g8 f4 | % 6
	  f4 e8 d8 c4 | % 7
	  a'4 g8 f8 e4 | % 8
	  c'4 b8 (a8) g8 f8 | % 9
	  e4 e8 f8 d4 | % 10
	  c2. ( | % 11
	  c2.) \bar ":|" % 12
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
	\clef bass
	\time 3/4
	\set Staff.instrumentName = "cello"
	\relative c {
	 e4-1 e d-0 | % 1
	 c2.-4 | % 2
	 e4-1 e d-0 | % 3
	 c2.-4 | % 4
	 e4-1 (f-3) g-4| % 5
	 g4-4 f8-3 e8-1 f4-3 | % 6
	 d4-0 (e-1) f-3 | % 7
	 f4 e8-1 d8-0 e4-1 | % 8
	 e4-1 e f-3 | % 9
	 g2.-4 | % 10
	 e4-1 e d-0 | % 11
	 c2.-4 \bar ":|" % 12
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "7. Winter Ade" } }
  }