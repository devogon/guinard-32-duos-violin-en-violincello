\tocItem \markup "28 Sherzlied"
  \score {

    \new GrandStaff <<
      \new Staff = "violin" {
	\key c \major
        \time 4/4


	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            \partial 4 r4 |   % 1
            r1 |              % 2
            r2 r4 c8^\markup { \small { Erstes Mal: Tacet } } d |      % 3
            e4 e8 e f4 e8 d | % 4
            e2 f4 e8 d |      % 5
            e2 f4 g8 a |      % 6
            g4 (e) e' d |     % 7
            c8 c c4 b a8 g |  % 8
            c2.              % 9
          }
          e4 |               % 10
          e8 d c4 b8 b a g | % 11
          <e c'>2. \bar "|." % 12
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \repeat volta 2 {
            \partial 4 g4-0     | % 1
            c4-4 c d-0 c8-4 b-3 | % 2
            c2-4 g4-0 g         | % 3
            c4-4 c d-0 c8-4 b-3 | % 4
            c2-4 g4-0\staccato (g\staccato) | % 5
            c4-4 c d8-0 d d d   | % 6
            e4-1 (g)-4 g f-3    | % 7
            e4-1 e d-0 c8-4 b-3 | % 8
            c2.-4                % 9
          }
          g4-0                  | % 10
          c8-4 d-0 e-1 f-3 g4-4 g,-0 | % 11
          c2.-4                 % 12
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "28 Sherzlied" } }
  }