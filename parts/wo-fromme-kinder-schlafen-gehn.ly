\tocItem \markup "31 Wo Fromme Kinder Schlafen Gehn"
  \score {
    % wiegenlied

    \new GrandStaff <<

      \new Staff = "violin" {
	\key g \major
        \time 4/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \repeat volta 2 {
            \partial 4 r4    | % 1
            r1               | % 2
            r2 r4 b          | % 3
            g2 e             | % 4
            g2. r4           | % 5
            a2 d4 (c8 b)     | % 6
            a2 d,4 (e8 fis)     | % 7
            g2 e               % 8
          }
          \alternative {
            { g2. }
            { \cadenzaOn

              d2\> b' \! \bar "|"  b2.\pp }
          }
          \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          \repeat volta 2 {
            \partial 4 b4-1    | % 1
            d4-4 (b-1) a-0 (e-1)     | % 2
            g4.-4 (fis8-3) fis4 (d-0)  | % 3
            e4-1 (fis-3) g-4 (fis-3)     | % 4
            g4-4 (a-0 b-1) r         | % 5
            d4-4 (c8-3 b-1) a2-0    | % 6
            d4-4 (c8-3 b-1) a2-0     | % 7
            g4-4 (fis8-3 g-4) b4-1 (fis8-3 e-1)  | % 8
            }
          \alternative {
            { 
              fis4.-3 (g8-4) g4 }
%            \cadenzaOn
            { fis2.-3 (g4-4) g2. }
            }
        }
      }
    >>
    \header { piece = \markup \bold { \caps "31 Wo Fromme Kinder Schlafen Gehn" } }
  }

  %          { \set Timing.measureLength = #(ly:make-moment 4/4) fis2.-3 (g4-4) 
%              \set Timing.measureLength = #(ly:make-moment 3/4) g2. }