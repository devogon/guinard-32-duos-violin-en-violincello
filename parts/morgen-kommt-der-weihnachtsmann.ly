\tocItem \markup " 4. Morgen Kommte Der Weihnachtsmann"
  \score {

    \new GrandStaff <<
      \new Staff = "violin" {
	\key g \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
				% music in here
	  \key g \major 
				%barkeysig: 
	  \key g \major 
				%bartimesig: 
	  \time 4/4 
	  g'8 fis g a b (ais) b4      | % 1
	  c8 (b) c4 (c8) b ais b      | % 2
	  a (gis) a4 g8 (fis) g4(      | % 3
	  g8) fis e fis g2      | % 4
	  b4 c8 (b) a (b) a4      | % 5
	  g4. (a8) g4 (fis)      | % 6
	  b c8 (b) a (b) a4      | % 7
	  g8 (fis g a) fis2      | % 8
	  g8 fis g a b (ais) b4      | % 9
	  c8 b c d c (b) b4      | % 10
	  a8 d, e fis g a b g    | % 11
	  fis8 e d fis g2 \bar "|." 
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
	\set Staff.instrumentName = "cello"
	\relative c {
	  \key g \major 
				%barkeysig: 
	  \key g \major 
				%bartimesig: 
	  \time 4/4 
	  g4-0 g d'-0 d      | % 1
	  e-1 e d2-0         | % 2
	  c4-4 c b-3 b       | % 3
	  a-1 a g2-0         | % 4
	  d'4-0 d c-4 c      | % 5
	  b-3 b b (a)-1      | % 6
	  d-0 d c-4 c        | % 7
	  b-3 b b (a)-1      | % 8
	  g-0 g d'-0 d       | % 9
	  e-1 e d2-0         | % 10
	  c4-4 c b-3 b       | % 11
	  a4-1 a g2-0 \bar "|."  | % 12
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "4. Morgen kommt der weihnachtsmann" } }
  }