\tocItem \markup "27 Altfranzösisches Menuett"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
        \time 3/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
		r2.  % 1
                r2. % 2
                \repeat volta 2 {
                  b4 (g4\staccato) g4\staccato | % 3
                  g2. | % 4
                  a4 (d,4\staccato) d4\staccato | % 5
                  d8 e fis g a4 | % 6
                  b4 (g4\staccato) g4\staccato | % 7
                  g2. | % 8
                  a2. (| % 9
                  a2) r4 | % 10
                  }
                \repeat volta 2 {
                  g4 fis8 g a b |  % 11
                  c4 b a | % 12
                  g4. g8 fis4 | % 13
                  e4 d2 | % 14
                  g4 fis8 g a b | % 15
                  c4 b a | % 16
                  g4 e8 g fis4 | % 17
                  g2. | % 18
                  }
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
        \time 3/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          b4-1 c8-3 b-1 a-0 g-4 | % 1
          fis4-3 (d2-0) | % 2
          \repeat volta 2 {
            g4-4 b-1 d-4 | % 3
            b4-1 c8-3 b-1 a-0 g-4 | % 4
            fis4-3 d-0 g-4 | % 5
            a8-0 g-4 a-0 b-1 a4-0 | % 6
            g4-4 b-1 d-4 | % 7
            b4-1 c8-3 b-1 a-0 g-4 | % 8
            fis4-3 d-0 g8-4 (a-0) | % 9
            a2 r4 | % 10
            }
          \repeat volta 2 {
            b4-1 c8-3 b-1 a-0 g-4 | % 11
            fis4-3 g-4 a-0 | % 12
            b4-1 c8-3 b-1 a-0 g-4 | % 13
            a8-0 g-4 a-0 b-1 a4-0 | % 14
            b4-1 c8-3 b-1 a-0 g-4 | % 15
            fis4-3 g-4 a-0 | % 16
            b4-1 c8-3 b-1 a-0 b-1 | % 17
            g2.-4 | % 18
          }
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "27 Altfranzösisches Menuett" } }
  }