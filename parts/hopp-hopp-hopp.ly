\tocItem \markup "15 Hopp, Hopp, Hopp!"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
	\set Staff.instrumentName = "violin"
	\clef treble
        \time 2/4
	\relative c' {
		r4 c |
                e4 g |
                r4 g8 fis |
                e8 d c4 |
                fis8 fis g d |
                g8 b c4 |
                fis,8 fis g d' |
                c8 r c,4 |
                e4 g |
                r4 g8 fis |
                e8 d c4 |
                r2 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
        \time 2/4
	\set Staff.instrumentName = "violoncello"
	\relative c {
          c4-4 e-1 |
          g4-4 r |
          g8 fis-3 e-1 d-0 |
          a4-1 r |
          d8-0 d b-3 g-0 |
          g8 g e-3 c-0 |
          d'8-0 d b-3 g-0 |
          g'8-4 g e-1 c-4 |
          c8 d-0 e-1 fis-3 |
          g4-4 r |
          g8 fis-3 e-1 d-0 |
          c2-4 \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "15 Hopp, Hopp, Hopp!" } }
  }