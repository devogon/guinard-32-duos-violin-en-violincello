\tocItem \markup "14 Finnländisches Kinderlied (Biiri, Biiri, Büeri)"
  \score {


    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
	\set Staff.instrumentName = "violin"
	\clef treble
        \time 2/4
	\relative c'' {
          g4. c8 |
          c4 b |
          b8 r g c |
          c8 d b4 |
          d4 f, |
          e8 f (g4) |
          g8 d4 f8 |
          e8 d c4 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
	\clef bass
        \time 2/4
	\set Staff.instrumentName = "violoncello"
	\relative c' {
          g8-4 e-1 c-4 e-1 |
          e4 d-0 |
          g8-4 e-1 c-4 e-1 |
          e4 d-0 |
          f8-3 f a-0 a |
          g8-4 g e4-1 |
          f8-3 f b,-3 b |
          c8-4 c c4 \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "14 Finnländisches Kinderlied (Biiri, Biiri, Büeri)" } }
  }