\tocItem \markup "29 Sommertag (this needs checking against book copy!)"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key f \major
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \repeat volta 2 {
            c8\staccato c\staccato c\staccato c\staccato | % 1
            c4 r | % 2
            c4 (c16) d c bes | % 3
            a8 (f) f4 | % 4 
            r8 d'8\staccato c\staccato bes\staccato | % 5
            g8\staccato f\staccato f4\staccato | % 6
            r8 c'8\staccato bes\staccato a\staccato | % 7
            g16 g f f e4 | % 8   e4 has a line underneath FIXME
            r2 | % 9
            r8 f\staccato a\staccato bes\staccato   % 10
            c4 (c16) d c bes |   % 11
            a8 ( f) f4 | % 12
          }
	}
      }
      
      \new Staff = "violoncello" {
	\key f \major
        \time 2/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c, {
          \repeat volta 2 {
            f4---4 a---1 |   % 1
            g4---0 r8 c,-0\staccato  |  % 2
            d8-1\staccato c-0\staccato d-1\staccato e-3\staccato |   % 3
            f4---4\staccato r8 c'-4\staccato   | % 4
            bes8-3\staccato a-1\staccato g-0\staccato f-4 | % 5
            e8-4\staccato a-1\staccato c,-0\staccato  c'-4\staccato  | % 6
            bes8-3 a-1\staccato g-0\staccato f-3\staccato | % 7
            e8-3\staccato g-0\staccato c,4-0 | % 8
            f4---3 a---1 | % 9
            c4-4 r8 bes-3\staccato  | % 10
            a8-1\staccato a\staccato g-0\staccato g\staccato | % 11
            f4---4\staccato  r | % 12
          }
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "29 Sommertag (this needs checking against book copy!)" } }
  }