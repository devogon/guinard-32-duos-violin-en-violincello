\tocItem \markup "18 Im Aargau Sind Zwöi Liebi"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key b \minor
        \time 4/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          f4 d' a2 |
		\repeat volta 2 {
                  fis4 d8 fis a2 (|
                  a2.) r4 |
                  r4 e8 g a2 (|
                  a2.) r4 |
                  r4 d,8 fis a2 (|
                  a4) g8 fis e4 fis8 e |
                  d2 r4 d8 e |
                  fis4 g8 a b4 a | }
          \alternative {
            { \time 3/2 b8 cis d4 g,8 a b4 e,8 fis g4 }
            { \time 4/4 d2 r }
          }
          \bar "|."

	}
      }
      
      \new Staff = "violoncello" {
	\key b \minor
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          r1 |
          \repeat volta 2 {
            r2 r4 d8-0 e-1 |
            fis4-3 fis fis e8-1 fis-3 |
            g4-4 e-1 r e8 fis-3 |
            g4-4 g g fis8-3 g-4 |
            a4-0 fis-3 r fis8 g-4 |
            a4-0 b8-1 a-0 g4-4 a8-0 g-4 |
            fis4-3 d'-4 a-0 fis8-3 g-4 |
            a4-0 b8-1 a-0 g4-4 a8-0 g-4 | }
          \alternative {
            { \time 3/2 fis2-3 r2 r2 }
            { \time 4/4 fis2-3 r2 }
            }
          \bar "|."
            }
	}
	

    >>
    \header { piece = \markup \bold { \caps "18 Im Aargau Sind Zwöi Liebi" } }
  }