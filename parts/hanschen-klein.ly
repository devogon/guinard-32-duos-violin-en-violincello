\tocItem \markup " 6. Hänschen Klein / Alles neu macht der mai"
  \score {
    % aka: lightly row
% andante cantabile
    \new GrandStaff <<
      \new Staff = "violin" {
	\key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\time 4/4
	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
	\relative c'' {
	  r2 d,4 (a') | % 1
	  a2 g4 (a) | % 2
	  fis (e) d d' | % 3
	  cis b a2 | % 4
	  r2 d,4 (a') | % 5
	  a2 g4 (a) | % 6
	  d2 cis2 | % 7
	  d2. r4 | % 8
	  cis cis cis b | % 9
	  a2 r2 | % 10
	  r4 d d b | % 11
	  a2 r2 | % 12
	  r2 d,4 (a') | % 13
	  a b a g | % 14
	  fis d fis e | % 15
	  d2. r4 \bar ":|" % 16
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "cello"
	\relative c' {
	  a4-0 (fis-3) fis2 | % 1
	  g4-4 (e-1) e2 | % 2
	  d4-0 (e-1) fis-3 g-4 | % 3
	  a4-0 a a2 | % 4
	  a4-0 (fis-3) fis2 | % 5
	  g4-4 (e-1) e2 | % 6
          d4-0 (fis-3) a-0 a | % 7
	  d,2.-0 r4 | % 8
	  e4-1 e e e | % 9
	  e-1 (fis-3) g2-4 | % 10
	  fis4-3 fis fis fis | % 11
	  fis-3 (g-4) a2-0 | % 12
	  a4-0 (fis-3) fis2 | % 13
	  g4-4 (e-1) e2 | % 14
	  d4-0 fis-3 a-0 a | % 15
	  d,2.-0 r4 \bar ":|" % 16
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "6. Hänschen Klein / Alles neu macht der mai" } }
  }