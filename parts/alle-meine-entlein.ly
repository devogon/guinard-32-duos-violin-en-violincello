\tocItem \markup " 3. Alle Meine Entlein"
  \score {
    \new GrandStaff <<
      \new Staff = "violin" {
	\key d \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
	r2 d4\staccato e4\staccato |
	fis4\staccato d4\staccato fis4\staccato d4\staccato |
	g4\staccato d4\staccato g4\staccato e4\staccato |
	fis4\staccato d4\staccato e4\staccato fis4\staccato |
	g4\staccato fis4\staccato g4\staccato d4\staccato |
	fis4\staccato e4\staccato d4\staccato r4 |
	r2 e4\staccato a4\staccato |
	d,4\staccato a'4\staccato d,4\staccato fis4\staccato |
	g4\staccato fis4\staccato g4\staccato a4\staccato |
	fis4\staccato e4\staccato d2 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key d \major
	\clef bass
	\set Staff.instrumentName = "cello"
	\relative d
	d4-0 e4-1 fis4-3 g4-4 |
	a2-0 a2 |
	b4-1 b4 b4 b4 |
	a1-0 |
	b4-1 b4 b4 b4 |
	a1-0 |
	g4-4 g4 g4 g4 |
	fis2-3 fis2 |
	e4-1 e4 e4 e4 |
	d1-0 |
      }
    >>
    \header { piece =  \markup \bold { \caps "3. Alle Meine Entlein" } }
  }