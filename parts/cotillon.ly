\tocItem \markup "32 Cotillon"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
        \time 4/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
          \repeat volta 2 {
            \partial 2 r4 g8 f |  % 1
            e8 d c 4 f8 e d4 |  % 2
            e8 d c c' b4 a8 g |  % 3
            c2. b8 a |  % 4
            b2   % 5
          }
          \repeat volta 2 {
            e8 (d) c e |  % 6
            f8 (e) d f    e (d) a e' |  % 7
            b2   e8 (f) e d |  % 8
            c4 a2 c4 \((| % 9
            c4) b\) e8 (d) c e | % 10
            d8 (c) b d    c (b) a c |  % 11
            b8 (a) g b   a (g) f a | % 12
          }
          \alternative {
            {g8 f e4   f8 (e) d f |  % 13
             e2 }    % 14
            {g8 f e4   f8 (e) d e   % 15
           c2\fermata }  % 16
          }
          \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
        \time 4/4
	\clef bass
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \repeat volta 2 {
            \partial 2 c8-4\upbow (d-0) e-1 f-3 | % 1
            g4-4 f8-3 g-4 a4-0 g8-4 f-3 | % 2
            g4-4 (e-1) g-4 f8-3 e-1 | % 3
            f4-3 e8-1 d-0 e4-1 d8-0 c-4 | % 4
            d2-0 % 5
            }
          \repeat volta 2 {
            g8-4 (f-3) e-1 g-4 | % 6
            f8-3 (e-1) d-0 f-3   e-1 (d-0) c-4 e-1 | % 7
            d4-0 (g,-0) c8-4 b-3 c-4 d-0 | % 8
            e4-1 f8-3 g-4 f4.-3 (e8-1) | % 9
            d2-0 g8-4 (f-3) e-1 g-4 | % 10
            f8-3 (e-1) d-0 f-3   e-1 (d-0) c-4 e-1 | % 11
            d4-0 (g,-0) c8-4 (b-3) c-4 d-0 | % 12
            }
          \alternative { {
            e4-1 f8-3 g-4 d4.-0 (c8-4) | % 13
            c2 } % 14
            { \cadenzaOn
              e4-1 f8-3 g-4 d4.-0 (c8-4)  \bar "|" % 15
            c4 c,-0\fermata }  % 16
          }
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "32 Cotillon" } }
  }


