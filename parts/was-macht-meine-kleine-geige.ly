\tocItem \markup "22 Was Macht Meine Kleine Geige"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key g \major
        \time 2/4
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c' {
          \repeat volta 2 {
            \partial 8 r8 | % 1
            r4 r8 d       | % 2
            g8 g g a      | % 3
            b8 a g4       | % 4
            d8 (c) b d    | % 5
            g8 g16 g g8 a | % 6
            b8 a g4       | % 7
            d'16 d c c b4 | % 8
            b8 a g4       | % 9
            fis4 r8 g       | % 10
            g4 e'         | % 11
            b4 a8 g       | % 12
            fis4 (a8)         % 13
            
            }
	}
      }
      
      \new Staff = "violoncello" {
	\key g \major
	\clef bass
        \time 2/4
	\set Staff.instrumentName = "violoncello"
	\relative c {
          \repeat volta 2 {
            \partial 8 d8-0\downbow | % 1
            g8-4 g g a-0       | % 2
            b8-1 a-0 g4-4        | % 3
            d'8-4 (c-3) a-0 d,-0   | % 4
            g8-4 g16 g g8 a-0  | % 5
            b8-1 a-0 g4-4        | % 6
            d'16-4 d c-3 c b4-1  | % 7
            b8 a-0 g4-4        | % 8
            g8-4 g c-3 b-1       | % 9
            a4-0 g-4           | % 10
            b16-1 b a-0 a g4-4   | % 11
            g8 g16 g c8-3 b-1  | % 12
            a4-0 (g8-4)          % 13
            }
	}
      }
    >>
    \header { piece = \markup \bold { \caps "22 Was Macht Meine Kleine Geige" } }
  }