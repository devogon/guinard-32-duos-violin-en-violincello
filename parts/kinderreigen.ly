\tocItem \markup " 9 Kinderreigen"
  \score {

    \new GrandStaff <<
%	\tempo \markup \teeny \italic {"Andante Cantabile" 4 = 75}
      \new Staff = "violin" {
	\key c \major
	\set Staff.instrumentName = "violin"
	\clef treble
	\relative c'' {
		g8 c b a g f e d |
                e c e f g2 |
                g8 c b a g c g f |
                e d e f g2 |
                g8 c b a g c b c |
                g c b a g a g f |
                e c d f g a g b |
                g c16 b a8 b c g e4 \bar "|."
	}
      }
      
      \new Staff = "violoncello" {
	\key c \major
	\clef bass
	\set Staff.instrumentName = "violincello"
	\relative c' {
          g4-4 g a-0 a |
          g2-4 e-1 |
          g4-4 g a-0 a |
          g2-4 e-1 |
          g4-4 g a-0 a |
          g4-4 g e2-1 |
          g4-4 f-3 e-1 c-4 |
          d-0 d c2-4 \bar "|."
	}
	
      }
    >>
    \header { piece = \markup \bold { \caps "9 Kinderreigen" } }
  }