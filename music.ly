\version "2.16.2"
#(set-global-staff-size 23)

% \paper {
%   paper-size = a4
%   }

\layout {
  }

\header {
  copyright = "G.H. 10975"
  title = \markup \huge {\caps "Eric Guignard" }
  subtitle = \markup \bold { \caps "32 kleine Duos" }
  subsubtitle = \markup \bold { \caps "Musik für Violoncello" }
  edition = \markup { "Edition Hug 10975" }
  maintainer = "daniël bosold"
  source = "erm"
  tagline = "G.H. 10975"
  % guignard\\ 32 duos \\ mit violoncello \\ edition hug 10975
  }
% \markup { \column {
% \huge \caps "Eric Guignard"
% "32 kleine Duos"
% "für ein Melodieinstrument"
% "und Violoncello"
% "(1. Lage)"
% "Musik Hug"
% }
% }
% include toc 
%\override #'(baseline-skip . 8.5)
\markuplist {"This music was transcribed from the original (no-longer-available) book by (me) daniël bosold."}
\markuplist {"Any errors are entirely my fault."}

\markuplist \table-of-contents

\pageBreak

\pageBreak
  \include "parts/wiegenlied.ly"

  \include "parts/heile-heile.ly"
  \include "parts/alle-meine-entlein.ly"
\pageBreak
  \include "parts/morgen-kommt-der-weihnachtsmann.ly"
  \include "parts/schneewittchen.ly"
\pageBreak
  \include "parts/hanschen-klein.ly"
  \include "parts/winter-ade.ly"
\pageBreak
  \include "parts/alle-vogel-sind-schon-da.ly"
  \include "parts/kinderreigen.ly"
\pageBreak
  \include "parts/summ-summ-summ.ly"
  \include "parts/ihr-kinderlein-kommet.ly"
\pageBreak
  \include "parts/auf-unsrer-wiese-gehet-was.ly"
  \include "parts/sur-le-pont-davignon.ly"
  \include "parts/finnlandisches-kinderlied-biiri-biiri-bueri.ly"
\pageBreak
  \include "parts/hopp-hopp-hopp.ly"
  \include "parts/liebe-schwester-tans-mit-mir.ly"
  \include "parts/kommt-ein-vogel.ly"
\pageBreak
  \include "parts/im-aargau-sind-zwoi-liebi.ly"
  \include "parts/kuku.ly"
\pageBreak
  \include "parts/aus-dem-himmel-ferne.ly"
  \include "parts/schneie.ly"
  \include "parts/was-macht-meine-kleine-geige.ly"
\pageBreak
  \include "parts/gott-ists-der-regiert.ly"
  \include "parts/menuett.ly"
\pageBreak
  \include "parts/jager-und-hase.ly"
  \include "parts/der-winter-ist-vergangen.ly"
\pageBreak
  \include "parts/altfranzosisches-menuett.ly"
  \include "parts/scherzlied.ly"
\pageBreak
  \include "parts/sommertag.ly"
  \include "parts/ich-hab-mir-mein-kindlein.ly"
\pageBreak
  \include "parts/wo-fromme-kinder-schlafen-gehn.ly"
  \include "parts/cotillon.ly"

